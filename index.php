<?php
/* Version:     12.1
    Date:       02/03/25
    Name:       index.php
    Purpose:    Main site page
    Notes:       
    To do:      
    
    1.0
                Initial version
 *  2.0     
 *              Moved image calls to use scryfall function
 *  3.0         
 *              Moved from writelog to Message class
 *  4.0
 *              Moved to mysqli
 *  5.0
 *              Re-factoring for cards_scry
 *              Javascript simplification and Ajax changes
 *  6.0
 *              Layout changes for Arena cards
 *  7.0
 *              Add flip capability for battle cards
 *  8.0
 *              Changes to handle etched cards
 *
 *  9.0         02/12/23
 *              Add javascript to add/remove b/w based on cview mode
 *
 * 10.0         09/12/23
 *              Move main search to parameterised queries
 * 
 * 11.0         02/01/24
 *              Add language search parameters
 * 
 * 11.1         20/01/24
 *              Move to logMessage
 * 
 * 11.2         22/01/24
 *              Increased $maxresults to 3,500 to cope with The List
 * 
 * 11.3         06/06/24
 *              Move interpretation of input field to global function
 *              This allows interpretation of e.g. "Farfinder [IKO 2]"
 * 
 * 11.4         10/06/24
 *              Add AND / OR to type searches
 * 
 * 11.5         09/12/24
 *              Move tribal valid list to ini.php
 * 
 * 12.0         01/03/25
 *              Add Name exact
 * 
 * 12.1         02/03/25
 *              Catch and evade empty ability search
*/

//Call script initiation mechs
if (file_exists('includes/sessionname.local.php')):
    require('includes/sessionname.local.php');
else:
    require('includes/sessionname_template.php');
endif;
startCustomSession();
require ('includes/ini.php');                //Initialise and load ini file
require ('includes/error_handling.php');
require ('includes/functions.php');      //Includes basic functions for non-secure pages
require ('includes/secpagesetup.php');       //Setup page variables
forcechgpwd();                               //Check if user is disabled or needs to change password
$msg = new Message($logfile);

// Default numbers per page and max
$listperpage = 30;
$gridperpage = 30;
$bulkperpage = 1000;
$maxresults = 3500;
$time = time();

// Is admin running the page
$msg->logMessage('[DEBUG]',"Admin is $admin");

// Define layout and results per page for each layout type
if (!empty($_GET)):
    $fullQueryString = $_SERVER['QUERY_STRING'];
    $msg->logMessage('[DEBUG]',"Query string: " . $fullQueryString);
else:
    $msg->logMessage('[DEBUG]',"Query string: none");
endif;

if (isset($_GET['layout'])):
    $valid_layout = array("grid","list","bulk");
    $layout = $_GET['layout'];
    if (!in_array($layout,$valid_layout)):
        $layout == 'grid';
    endif;
    if ($layout == 'grid'):
        $perpage = $gridperpage;
    elseif ($layout == 'list'):
        $perpage = $listperpage;
    elseif ($layout == 'bulk'):
        $perpage = $bulkperpage;
    else:
        $layout = 'grid';
        $perpage = $gridperpage;
    endif;
else:
    $layout = 'grid'; //default to grid if not specified
    $perpage = $gridperpage;
endif;

// Set up all the stuff we need and filter GET variables
if (isset($_GET["page"])) :
    $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
else :
    $page = 1;
endif;
$perpage = (int)$perpage;
$start_from = ($page - 1) * $perpage;
$start_from = (int)$start_from;
if (isset($_GET['name']) AND $_GET['name'] !== ""):
    $nameget = htmlspecialchars($_GET["name"],ENT_NOQUOTES);
    $msg->logMessage('[DEBUG]',"Name in GET is $nameget");
    $nametrim = trim($nameget, " \t\n\r\0\x0B");
    $msg->logMessage('[DEBUG]',"Name after trimming is $nametrim");
    $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
    $name = preg_replace($regex, ' ', $nametrim);
    $msg->logMessage('[DEBUG]',"Name after URL removal is $name");
    $interpreted_string = input_interpreter($name);
    if (isset($interpreted_string['name']) AND $interpreted_string['name'] !== ''):
        $name = $interpreted_string['name'];
    else:
        $name = '';
    endif;
    // Set
    if (isset($interpreted_string['set']) AND $interpreted_string['set'] !== ''):
        $setcoderegexsearch = strtoupper($interpreted_string['set']);
    else:
        $setcoderegexsearch = '';
    endif;
    // Collector number
    if (isset($interpreted_string['number']) AND $interpreted_string['number'] !== ''):
        $numberregexsearch = $interpreted_string['number'];
    else:
        $numberregexsearch = '';
    endif;
    $card = htmlspecialchars_decode($name,ENT_QUOTES);
    $name = trim(preg_replace('/\[[A-Za-z0-9]+\]/', '', $name));
else:
    $name = '';
    $setcoderegexsearch = '';
    $numberregexsearch = '';
endif;
$searchname = isset($_GET['searchname']) ? 'yes' : '';
$searchtype = isset($_GET['searchtype']) ? 'yes' : '';
$searchsetcode = isset($_GET['searchsetcode']) ? 'yes' : '';
$searchability = isset($_GET['searchability']) ? 'yes' : '';
$searchnotes = isset($_GET['searchnotes']) ? 'yes' : '';
$searchpromo = isset($_GET['searchpromo']) ? 'yes' : '';
$new = isset($_GET['searchnew']) ? 'yes' : '';
$white = isset($_GET['white']) ? 'yes' : '';
$blue = isset($_GET['blue']) ? 'yes' : '';
$black = isset($_GET['black']) ? 'yes' : '';
$red = isset($_GET['red']) ? 'yes' : '';
$green = isset($_GET['green']) ? 'yes' : '';
$artifact = isset($_GET['artifact']) ? 'yes' : '';
$colourless = isset($_GET['colourless']) ? 'yes' : '';
$land = isset($_GET['land']) ? 'yes' : '';
$battle = isset($_GET['battle']) ? 'yes' : '';
$valid_colourOp = array("AND","OR","");
$colourOp = isset($_GET['colourOp']) ? "{$_GET['colourOp']}" : '';
if (!in_array($colourOp,$valid_colourOp)):
    $colourOp = '';
endif;
$colourExcl = isset($_GET['colourExcl']) ? 'ONLY' : '';
$valid_typeOp = array("AND","OR","");
$typeOp = isset($_GET['typeOp']) ? "{$_GET['typeOp']}" : '';
if (!in_array($typeOp,$valid_typeOp)):
    $typeOp = '';
endif;
$common = isset($_GET['common']) ? 'yes' : '';
$uncommon = isset($_GET['uncommon']) ? 'yes' : '';
$rare = isset($_GET['rare']) ? 'yes' : '';
$mythic = isset($_GET['mythic']) ? 'yes' : '';
$paper = isset($_GET['paper']) ? 'yes' : '';
$arena = isset($_GET['arena']) ? 'yes' : '';
$online = isset($_GET['online']) ? 'yes' : '';
$valid_gametypeOp = array("AND","OR","");
$gametypeOp = isset($_GET['gametypeOp']) ? "{$_GET['gametypeOp']}" : '';
if (!in_array($gametypeOp,$valid_gametypeOp)):
    $gametypeOp = '';
endif;
$gametypeExcl = isset($_GET['gametypeExcl']) ? 'ONLY' : '';
$online = isset($_GET['online']) ? 'yes' : '';
$creature = isset($_GET['creature']) ? 'yes' : '';
$instant = isset($_GET['instant']) ? 'yes' : '';
$sorcery = isset($_GET['sorcery']) ? 'yes' : '';
$enchantment = isset($_GET['enchantment']) ? 'yes' : '';
$planeswalker = isset($_GET['planeswalker']) ? 'yes' : '';
$tribal = isset($_GET['tribal']) ? 'yes' : '';
$tribe = isset($_GET['tribe']) ? "{$_GET['tribe']}" : '';
if (!in_array($tribe,$valid_tribe)):
    $tribe = '';
endif;
$legendary = isset($_GET['legendary']) ? 'yes' : '';
$token = isset($_GET['token']) ? 'yes' : '';
# $rareOp = isset($_GET['rareOp']) ? filter_input(INPUT_GET, 'rareOp', FILTER_SANITIZE_STRING):'';
$exact = isset($_GET['exact']) ? 'yes' : '';
$allprintings = isset($_GET['allprintings']) ? 'yes' : '';
if ((isset($_GET['set'])) AND ( is_array($_GET['set']))):
    $selectedSets = filter_var_array($_GET['set'], FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_NO_ENCODE_QUOTES);
endif;
$valid_sortBy = array("name","price","cmc","cmcdown","set","setdown","setnumberdown","powerup","powerdown","toughup","toughdown","auto");
$sortBy = isset($_GET['sortBy']) ? "{$_GET['sortBy']}" : '';
if (!in_array($sortBy,$valid_sortBy)):
    $sortBy = '';
endif;
$valid_operator = ["ltn", "gtr", "eq"];
$poweroperator = isset($_GET['poweroperator']) ? "{$_GET['poweroperator']}" : '';
if (!in_array($poweroperator,$valid_operator, true)):
    $poweroperator = '';
endif;
$toughoperator = isset($_GET['toughoperator']) ? "{$_GET['toughoperator']}" : '';
if (!in_array($toughoperator,$valid_operator, true)):
    $toughoperator = '';
endif;
$loyaltyoperator = isset($_GET['loyaltyoperator']) ? "{$_GET['loyaltyoperator']}" : '';
if (!in_array($loyaltyoperator,$valid_operator, true)):
    $loyaltyoperator = '';
endif;
$cmcoperator = isset($_GET['cmcoperator']) ? "{$_GET['cmcoperator']}" : '';
if (!in_array($cmcoperator,$valid_operator, true)):
    $cmcoperator = '';
endif;
$collqtyoperator = $_GET['collQtyOp'] ?? '';
if (!in_array($collqtyoperator, $valid_operator, true)):
    $collqtyoperator = '';
endif;
$cmcvalue = isset($_GET['cmcvalue']) ? filter_input(INPUT_GET, 'cmcvalue', FILTER_SANITIZE_NUMBER_INT):'';
$power = filter_input(INPUT_GET, 'power', FILTER_VALIDATE_INT, [
    'options' => ['default' => 0, 'min_range' => 0] // Ensures a valid, non-negative integer
]);
$tough = filter_input(INPUT_GET, 'tough', FILTER_VALIDATE_INT, [
    'options' => ['default' => 0, 'min_range' => 0] // Ensures a valid, non-negative integer
]);
$loyalty = filter_input(INPUT_GET, 'loyalty', FILTER_VALIDATE_INT, [
    'options' => ['default' => 0, 'min_range' => 0] // Ensures a valid, non-negative integer
]);
$collqty = filter_input(INPUT_GET, 'collQtyValue', FILTER_VALIDATE_INT, [
    'options' => ['default' => 0, 'min_range' => 0] // Ensures a valid, non-negative integer
]);
$mytable = $user . "collection";
$adv = isset($_GET['complex']) ? 'yes' : '';
$scope = isset($_GET['scope']) ? "{$_GET['scope']}" : '';
$valid_scope = array("all","mycollection","notcollection");
if (!in_array($scope,$valid_scope)):
    $scope = '';
endif;
$valid_legal = array("std","pnr","mdn","vin","lgc","alc","his");
$legal = isset($_GET['legal']) ? "{$_GET['legal']}" : '';
if (!in_array($legal,$valid_legal)):
    $legal = '';
endif;
$foilonly = isset($_GET['foilonly']) ? 'yes' : '';
$searchLang = isset($_GET['lang']) ? "{$_GET['lang']}" : '';
if ($searchLang === 'all'):
    $searchLang = 'all';
elseif ($searchLang === 'default' || !in_array($searchLang,$search_langs_codes)):
    $searchLang = '';
endif;

// Does the user have a collection table?
$tableExistsQuery = "SHOW TABLES LIKE '$mytable'";
$msg->logMessage('[DEBUG]', "Checking if user has a collection table...");

$result = $db->query($tableExistsQuery);
if ($result->num_rows == 0):
    $msg->logMessage('[NOTICE]', "No existing collection table...");
    $query2 = "CREATE TABLE `$mytable` LIKE collectionTemplate";
    $msg->logMessage('[DEBUG]', "Copying collection template...: $query2");

    if ($db->query($query2) === TRUE):
        $msg->logMessage('[NOTICE]', "Collection template copy successful");
    else:
        $msg->logMessage('[NOTICE]', "Collection template copy failed: " . $db->error);
    endif;
else:
    $msg->logMessage('[DEBUG]', "Collection table exists");
endif;
    
// More general query building:
$selectAll = "SELECT 
                cards_scry.id as cs_id,
                price,
                price_foil,
                price_etched,
                price_sort,
                setcode,
                `$mytable`.normal,
                `$mytable`.foil,
                `$mytable`.etched,
                number,
                number_import,
                cards_scry.name,
                game_types,
                finishes,
                cards_scry.foil as cs_foil,
                cards_scry.nonfoil as cs_normal,
                cards_scry.release_date,
                sets.release_date as set_date,
                rarity,
                cards_scry.set_name,
                type,
                ability,
                manacost,
                layout,
                p1_component,
                p2_component,
                p3_component,
                p1_name,
                p2_name,
                p3_name,
                lang
                FROM cards_scry
                LEFT JOIN `$mytable` ON cards_scry.id = `$mytable`.id
                LEFT JOIN `sets` ON cards_scry.setcode = sets.code
                WHERE ";
$sorting = "LIMIT $start_from, $perpage";
require('includes/criteria.php'); //Builds $criteria and assesses validity
// If search is Mycollection / Sort By Price: 
// Update pricing in case any new cards have been added to collection
if (($sortBy == 'price') AND ( $scope == 'mycollection')):
    $msg->logMessage('[NOTICE]',"My Collection / Price query called, updating collection pricing");
    $obj = new PriceManager($db,$logfile,$useremail);
    $obj->updateCollectionValues($mytable);
endif;
//Set variable to ignore maxresults if this is a collection search
if ( $scope == 'mycollection' OR $sortBy == 'price'): // Price search waives the limit
    $collectionsearch = true;
else:
    $collectionsearch = false;
endif;    
// Run the query
if ($validsearch === "true"):
    $msg->logMessage('[DEBUG]',"User $useremail called query $query from {$_SERVER['REMOTE_ADDR']}");
    $msg->logMessage('[DEBUG]',"with parameters: ".var_export($params, true));
    // parameterised query has been built in criteria.php, proceed with it
    if($result = $db->execute_query($query, $params)):
        $msg->logMessage('[DEBUG]',"SQL query succeeded");
        $queryQty = "SELECT COUNT(*) FROM cards_scry LEFT JOIN `$mytable` ON cards_scry.id = `$mytable`.id WHERE ".$criteria;
        $msg->logMessage('[DEBUG]',"User $useremail called count query $queryQty");
        // Execute the count query
        if ($countResult = $db->execute_query($queryQty, $params)):
            $row = $countResult->fetch_row();
            $qtyresults = $row[0];
            $msg->logMessage('[DEBUG]',"Query has $qtyresults results");

            if ($qtyresults > $maxresults AND $collectionsearch == false):
                $validsearch = "toomany"; //variable set for header.php to display warning
            endif;
        else:
            trigger_error("[ERROR]".basename(__FILE__)." ".__LINE__.": SQL failure: " . $db->error, E_USER_ERROR);
        endif;
    endif;
else:
    $msg->logMessage('[DEBUG]', "Not a valid search");
endif;
# query for page navigation
if (isset($qtyresults)):
    if ($qtyresults > ($page * $perpage)):
        $next = $page + 1;
    endif;
    // Work out number of results, and pages
    if (($qtyresults - $start_from) <= $perpage) :
        $lastresult = $qtyresults;
    else :
        $lastresult = $page * $perpage;
    endif;
    $totalpages = $qtyresults / $perpage;
endif;
// Get the current GET string, less the layout and page keys if in there
// also run input through htmlspecialchars (via function)
$getstringbulk = getStringParameters($_GET, 'layout', 'page');

// Page layout starts here
$msg->logMessage('[DEBUG]',"Loading page layout");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1">
        <title> <?php echo $siteTitle;?> </title>
        <link rel="manifest" href="manifest.json" />
        <link rel="stylesheet" type="text/css" href="css/style<?php echo $cssver ?>.css">
        <?php include('includes/googlefonts.php'); ?>
        <script src="/js/jquery.js"></script>
        <script type="text/javascript">
            // Setup search form radio button exclusions
            $(function() {
                function setupMutualExclusion(selectorA, selectorB) {
                    $(selectorA).click(function () {
                        if ($(this).is(':checked')) {
                            $(selectorB).prop('checked', false).trigger('change');
                        }
                    });
                }

                setupMutualExclusion('#searchsetcode', '.notsetcode');
                setupMutualExclusion('#cb1', '.notname');
                setupMutualExclusion('#cb2', '#cb1');
                setupMutualExclusion('#abilitymain', '.notability');
                setupMutualExclusion('#yesnotes', '.notnotes');
                setupMutualExclusion('#searchpromo', '.notpromo');
                setupMutualExclusion('.notnotes', '#yesnotes');
                setupMutualExclusion('.notsetcode', '#searchsetcode');
                setupMutualExclusion('.notpromo', '#searchpromo');
                setupMutualExclusion('#abilityall', '.notability');
                setupMutualExclusion('.notability', '#abilityall');

                $('.scopecheckbox').click(function () {
                    if ($('.scopecheckbox:checked').length === 0) {
                        $('#cb1').prop('checked', true).trigger('change');
                    }
                });
                $('.gametypebox ').click(function () {
                    if ($('.gametypebox:checked').length === 0) {
                        $('#cb27').prop('checked', true).trigger('change');
                    }
                });
            });
        </script>

        <?php
        if ((isset($qtyresults)) AND ( $qtyresults != 0)): //Only load these scripts if this is a results call
            // Only load IAS if results are more than a page-full per page type
            if( ($layout == 'bulk' AND ( $qtyresults > $bulkperpage)) OR ($layout == 'list' AND ( $qtyresults > $listperpage)) OR ($layout == 'grid' AND ( $qtyresults > $gridperpage) AND (isset($validsearch) AND ($validsearch !== "toomany")))  ) :   
                // IAS will be needed ?>
                <script src="/js/infinite-ajax-scroll.min.js"></script>
                <script type="text/javascript">
                $(document).ready(function () { // Infinite Ajax Scroll configuration
                    let ias = new InfiniteAjaxScroll('.wrap', {
                        item: '.item', // single items
                        next: '.next',
                        pagination: '.pagination', // page navigation
                        negativeMargin: 250,
                        spinner: {
                            element: '.spinner',
                            delay: 600,
                            show: function(element) {
                                element.style.opacity = '1';
                            },
                            hide: function(element) {
                                element.style.opacity = '0';
                            }
                        }
                    });
                    ias.on('page', (event) => {
                        $(".top").show(200);
                    });
                    ias.on('last', function() {
                        let el = document.querySelector('.ias-no-more');
                        el.style.opacity = '1';
                    });
                    // update title and url when scrolling through pages
                    ias.on('page', (e) => {
                        document.title = e.title;
                        let state = history.state;
                        history.replaceState(state, e.title, e.url);
                    });
                });
                </script>   
                <?php
            else: //Results > 0 but < a page, show the No More Results footer at the end ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        let el = document.querySelector('.ias-no-more');
                        el.style.opacity = '1';
                    });
                </script> <?php
            endif; ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    document.body.style.cursor='default';
                    $(".top").hide();
                    var UrlVars = getUrlVars();
                    if (UrlVars["page"] > 1) {
                        $(".top").show();
                    }
                });

                function isInteger(x) {
                    return x % 1 === 0;
                };

                function getUrlVars(){
                    var vars = [], hash; 
                    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&'); // cut the URL string down to just everything after the ?, split this into an array with information like this: array [0] = "var=xxx"; array [1] = "var2=yyy";
                    //loop through each item in that array
                    for(var i = 0; i < hashes.length; i++)
                    {   //split the item at the "="
                        hash = hashes[i].split('=');
                        //put the value name of the first variable into the "vars" array
                        vars.push(hash[0]);
                        //assign the value to the variable name, now you can access it like this:
                        // alert(vars["var1"]); //alerts the value of the var1 variable from the url string
                        vars[hash[0]] = hash[1];
                    }
                    return vars;
                };
            </script> <?php
            if(($layout == 'grid' || $layout == 'bulk') AND isset($validsearch) AND ($validsearch !== "toomany")):  
                // Load ajax grid update JS ?>
                <script src="/js/ajaxUpdate.js"></script> <?php
            endif; 
            if($layout == 'grid' AND isset($validsearch) AND ($validsearch !== "toomany")):  
                $floating_button = true; 
                // Load script to manage toggle of classes to show B&W (Collection View)?>
                <script src="/js/cviewClassToggle.js"></script> <?php
            endif; 
        endif;?>
    </head>

    <body> <?php 
        include_once("includes/analyticstracking.php");
        $getString = getStringParameters($_GET, 'page'); ?>
        <div class="top"> <?php 
            echo "<a id='prevlink' href='index.php{$getString}&amp;page=1'>&nbsp;</a>"; ?>
        </div>
        <?php
        require('includes/overlays.php'); //menus
        require('includes/header.php');  //build header
        require('includes/menu.php'); //mobile menu

        if ((isset($qtyresults)) AND ( $qtyresults != 0)) : //Display Bulk / List / Grid menus and results header row
            if ($layout == 'bulk') :
                require('includes/bulkmenus.php');
            elseif ($layout == 'list') :
                require('includes/listmenus.php');
            elseif ($layout == 'grid') :
                require('includes/gridmenus.php');
            endif;
        else: ?>
            <script>
                // Function to toggle the visibility of the info box
                function toggleInfoBox() {
                    var infoBox = document.getElementById("infoBox");
                    infoBox.style.display = (infoBox.style.display === "none" || infoBox.style.display === "") ? "block" : "none";
                }
            </script>

            <!-- Hovering help button -->
            <div id="info-button" onclick="toggleInfoBox()">
                <span id="help-button" class="material-symbols-outlined">help</span>
            </div>

            <!-- Info box -->
            <div class="info-box" id="infoBox" style="display:none">
                <span class="close-button material-symbols-outlined" onclick="toggleInfoBox()">close</span>
                <div class="info-box-inner">
                    <h2 class="h2-no-top-margin">Search help</h2>
                    <br><b>Header search</b><br>
                    Header search will update live results as text is input.<br>
                    Use square brackets or parentheses to shortcut to a specific set or card, e.g.:<br>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<i>Goblin [m13]</i>
                    or
                    <i>[m13 215]</i>
                    or
                    <i>(m13 215)</i>
                    or
                    <i>(m13) 215</i>
                    <br><br>
                    <b>Languages</b><br>
                    By default, the primary language printing for each card will be returned.<br>
                    Use the language drop-down to search for other languages.
                    <br>
                    <br><b>Advanced search</b><br>
                    [ ] shortcut patterns will work in the main search to search with a name, or ability, etc., e.g. 
                    selecting Ability search and:<br>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;<i>Haste [m13]</i>
                    <br><br>
                    ...will return cards with the haste keyword from M13.
                    <br><br>
                    <b>Other search tips</b><br>
                    "New (7d)" will return cards added in the last 7 days.<br>
                    "Promo" will search card promo types, e.g. rainbow foil, etc.
                </div>
            </div> <?php
        endif;
        ?>
        <div id='page'> 
            <span id="printtitle" class="headername">
                <img src="images/white_m.png"><?php echo $siteTitle;?>
            </span>
            <?php
            if ((isset($qtyresults)) AND ( $qtyresults != 0)):
                if ($layout == 'bulk') : ?>
                    <div id="resultsgrid" class='wrap'>
                        <?php
                        while ($row = $result->fetch_array(MYSQLI_BOTH)): //$row now contains all card info
                            $msg->logMessage('[DEBUG]',"Current card: {$row['cs_id']}");
                            $setcode = strtolower($row['setcode']);
                            $scryid = $row['cs_id'];
                            if(isset($row['finishes'])):
                                $finishes = json_decode($row['finishes'], TRUE);
                                $cardtypes = cardtypes($finishes);
                            else:
                                $finishes = null;
                                $cardtypes = 'none';
                            endif;
                            $msg->logMessage('[DEBUG]',"Current card: {$row['cs_id']} is $cardtypes");
                            if (strpos($row['game_types'], 'paper') == false):
                                $not_paper = true;
                            else:
                                $not_paper = false;
                            endif;
                            $uppercasesetcode = strtoupper($setcode);
                            if(($row['p1_component'] === 'meld_result' AND $row['p1_name'] === $row['name']) OR ($row['p2_component'] === 'meld_result' AND $row['p2_name'] === $row['name']) OR ($row['p3_component'] === 'meld_result' AND $row['p3_name'] === $row['name'])):
                                $meld = 'meld_result';
                            elseif($row['p1_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part'):
                                $meld = 'meld_part';
                            else:
                                $meld = '';
                            endif;
                            // If the current record has null fields set the variables to 0 so updates
                            // from the Grid work.
                            if (empty($row['normal'])):
                                $myqty = 0;
                            else:
                                $myqty = $row['normal'];
                            endif;
                            if (empty($row['foil'])):
                                $myfoil = 0;
                            else:
                                $myfoil = $row['foil'];
                            endif;
                            if (empty($row['etched'])):
                                $myetch = 0;
                            else:
                                $myetch = $row['etched'];
                            endif;
                            if(($myqty + $myfoil + $myetch) > 0):
                                $in_collection = ' in_collection';
                            else:
                                $in_collection = '';
                            endif;
                            ?>
                            <div class='gridbox gridboxbulk item'><?php
                                if(stristr($row['name'],' // ') !== false):
                                    $bulkname = substr($row['name'], 0, strpos($row['name'], " // "))." //...";
                                else:
                                    $bulkname = $row['name'];
                                endif;
                                if(strlen($bulkname) > 17):
                                    $bulkname = substr($bulkname,0,17)."...";
                                endif;
                                $displayName = htmlspecialchars($row['name'], ENT_QUOTES, 'UTF-8');
                                $displayLang = strtoupper(htmlspecialchars($row['lang'], ENT_QUOTES, 'UTF-8'));
                                $csId = htmlspecialchars($row['cs_id'], ENT_QUOTES, 'UTF-8');
                                echo "&nbsp;&nbsp;<a title='$displayName ($displayLang)' class='gridlinkbulk' href='/carddetail.php?id=$csId' tabindex='-1'>{$uppercasesetcode} {$row['number']} $bulkname ($displayLang)</a>";
                                $cellid = "cell".$scryid;
                                $cellid_one = $cellid.'_one';
                                $cellid_two = $cellid.'_two';
                                $cellid_three = $cellid.'_three';
                                $cellid_one_flash = $cellid_one;
                                $cellid_two_flash = $cellid_two;
                                $cellid_three_flash = $cellid_three;
                                ?>
                                <table class='bulksubmittable'>
                                    <tr class='bulksubmitrow'>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_one"; ?>">
                                            <?php
                                            if($meld === 'meld_result'):
                                                echo "Meld card";
                                            elseif ($not_paper == true):
                                                echo "<i>MtG Arena/Online</i>";
                                            elseif ($cardtypes === 'foilonly'):
                                                $poststring = 'newfoil';
                                                echo "Foil: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myfoil' value='$myfoil' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myfoil\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            elseif ($cardtypes === 'etchedonly'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myfoil' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myetch\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                $poststring = 'newqty';
                                                echo "Normal: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myqty' value='$myqty' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myqty\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            endif;?>
                                        </td>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_two"; ?>">
                                            <?php
                                            if($meld === 'meld_result'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'foilonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'normalonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'etchedonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'normaletched'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_two' type='number' step='1' min='0' name='myetch' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_two\",\"$myetch\",\"$cellid_two_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                $poststring = 'newfoil';
                                                echo "Foil: <input class='bulkinput' id='$cellid_two' type='number' step='1' min='0' name='myfoil' value='$myfoil' onchange='ajaxUpdate(\"$scryid\",\"$cellid_two\",\"$myfoil\",\"$cellid_two_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            endif;?>
                                        </td>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_three"; ?>">
                                            <?php
                                            if ($cardtypes === 'normalfoiletched'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_three' type='number' step='1' min='0' name='myetch' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_three\",\"$myetch\",\"$cellid_three_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                echo "&nbsp;";
                                            endif;?>
                                        </td>
                                    </tr>
                                </table>
                            </div> <?php 
                        endwhile; ?>
                        <div class="ias-no-more">NO MORE RESULTS
                        </div>
                        <div class="spinner"><img src='/images/ajax-loader.gif' alt="LOADING">
                        </div>
                        <!--page navigation--> <?php
                        if (isset($next)):
                            $getString = getStringParameters($_GET, 'page');
                            ?>
                            <div class="pagination"> <?php echo "<a href='index.php{$getString}&amp;page=$next' class='next'>Next</a>"; ?>
                            </div> <?php 
                        endif ?>
                        <table class='bottompad'>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>    
                    </div> <?php
                elseif ($layout == 'list'):?>
                    <div id='results' class='wrap'>
                        <?php
                        while ($row = $result->fetch_array(MYSQLI_BOTH)) : 
                            $msg->logMessage('[DEBUG]',"Current card: {$row['cs_id']}");
                            $scryid = $row['cs_id']; ?>
                            <div class='item' style="cursor: pointer;" onclick="location.href='carddetail.php?id=<?php echo $scryid;?>';">
                                <table> <?php
                                    $msg->logMessage('[DEBUG]',"Current card: $scryid");
                                    $setcode = strtolower($row['setcode']);
                                    $uppercasesetcode = strtoupper($setcode);
                                    $scryid = $row['cs_id'];
                                    if(($row['p1_component'] === 'meld_result' AND $row['p1_name'] === $row['name']) OR ($row['p2_component'] === 'meld_result' AND $row['p2_name'] === $row['name']) OR ($row['p3_component'] === 'meld_result' AND $row['p3_name'] === $row['name'])):
                                        $meld = 'meld_result';
                                    elseif($row['p1_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part'):
                                        $meld = 'meld_part';
                                    else:
                                        $meld = '';
                                    endif;
                                    if(strlen($row['name']) > 14):
                                        $listname = substr($row['name'],0,14)."...";
                                    else:
                                        $listname = $row['name'];
                                    endif;
                                    $displayName = htmlspecialchars($row['name'], ENT_QUOTES, 'UTF-8');
                                    $displayLang = strtoupper(htmlspecialchars($row['lang'], ENT_QUOTES, 'UTF-8'));
                                    $csId = htmlspecialchars($row['cs_id'], ENT_QUOTES, 'UTF-8');
                                    ?>
                                    <tr class='resultsrow'>
                                        <td title='<?php echo "$displayName ($displayLang)" ?>' class="valuename"> <?php echo "$listname ($displayLang)"; ?> </td>    
                                            <?php
                                            if(isset($row['manacost']) AND !empty($row['manacost'])):
                                                $manac = symbolreplace($row['manacost']);
                                            else:
                                                $manac = NULL;
                                            endif;
                                            ?>
                                        <td class="valuerarity"> <?php echo ucfirst($row['rarity']); ?> </td>
                                        <td class="valueset"> <?php echo $row['set_name']; ?> </td>
                                        <td class="valuetype"> <?php echo $row['type']; ?> </td>
                                        <td class="valuenumber"> <?php echo $row['number']; ?> </td>
                                        <td class="valuemana"> <?php echo $manac; ?> </td>
                                        <td class="valuecollection">
                                            <?php
                                            echo $row['normal'] + $row['foil'] + $row['etched'];
                                            ?>
                                        </td>
                                        <td class="valueabilities"> 
                                            <?php
                                            if(isset($row['ability']) AND !empty($row['ability'])):
                                                $ability = symbolreplace($row['ability']);
                                                echo $ability;
                                            endif;
                                            ?> 
                                        </td>
                                    </tr>
                                </table>
                            </div>
                  <?php endwhile; ?>
                        <div class="ias-no-more">NO MORE RESULTS</div>
                        <div class="spinner"><img src='/images/ajax-loader.gif' alt="LOADING"></div>
                        <!--page navigation-->
                        <?php
                        if (isset($next)):
                            $getString = getStringParameters($_GET, 'page');
                            ?>
                            <div class="pagination"> <?php echo "<a href='index.php{$getString}&amp;page=$next' class='next'>Next</a>"; ?>
                            </div>
                        <?php endif ?>
                        <table class='bottompad'>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>    
                    </div> <?php 
                elseif ($layout == 'grid') :?>
                    <script type="text/javascript">
                        function swapImage(img_id, card_id, imageurl, imagebackurl) {
                            var $ImageId = $('#' + img_id);

                            if (!$ImageId.hasClass('flipped')) {
                                // If not flipped, apply flip effect
                                $ImageId.addClass('flipped');

                                // Set a timeout for half of the transition duration
                                setTimeout(function () {
                                    $ImageId.attr('src', imagebackurl);
                                }, 80);
                            } else {
                                // If already flipped, remove flip effect
                                $ImageId.removeClass('flipped');

                                // Set a timeout for half of the transition duration
                                setTimeout(function () {
                                    $ImageId.attr('src', imageurl);
                                }, 80);
                            }
                        };

                        function rotateImg(img_id) {
                            var $img = $('#' + img_id);
                            var currentTransform = $img.css('transform');

                            if (currentTransform === 'none' || currentTransform === '') {
                                $img.css('transform', 'rotate(180deg)');
                            } else {
                                $img.css('transform', 'none');
                            }
                        };
                    </script>
                    <div id="resultsgrid" class='wrap'>
                        <?php
                        $x = 1;
                        while ($row = $result->fetch_array(MYSQLI_BOTH)): 
                            $flipbutton = $row['cs_id']."flip";
                            $img_id = $row['cs_id']."img";
                            $setcode = strtolower($row['setcode']);
                            $scryid = $row['cs_id'];
                            if(isset($row['finishes'])):
                                $finishes = json_decode($row['finishes'], TRUE);
                                $cardtypes = cardtypes($finishes);
                            else:
                                $finishes = null;
                                $cardtypes = 'none';
                            endif;
                            $msg->logMessage('[DEBUG]',"Current card: {$row['cs_id']} is $cardtypes");
                            if (strpos($row['game_types'], 'paper') == false):
                                $not_paper = true;
                            else:
                                $not_paper = false;
                            endif;
                            $uppercasesetcode = strtoupper($setcode);
                            if(($row['p1_component'] === 'meld_result' AND $row['p1_name'] === $row['name']) OR ($row['p2_component'] === 'meld_result' AND $row['p2_name'] === $row['name']) OR ($row['p3_component'] === 'meld_result' AND $row['p3_name'] === $row['name'])):
                                $meld = 'meld_result';
                            elseif($row['p1_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part' OR $row['p2_component'] === 'meld_part'):
                                $meld = 'meld_part';
                            else:
                                $meld = '';
                            endif;
                            $imageManager = new ImageManager($db, $logfile, $serveremail, $adminemail);
                            $imagefunction = $imageManager->getImage($setcode,$row['cs_id'],$ImgLocation,$row['layout'],$two_card_detail_sections);
                            if($imagefunction['front'] == 'error'):
                                $imageurl = '/cardimg/back.jpg';
                            else:
                                $imageurl = $imagefunction['front'];
                            endif;
                            //If page is being loaded by admin, don't cache the image
                            if(($admin == 1) AND ($imageurl !== '/cardimg/back.jpg')):
                                $msg->logMessage('[DEBUG]',"Admin loading, don't cache image");
                                $imageurl = $imageurl.'?='.$time;
                            endif;
                            if(!is_null($imagefunction['back'])):
                                if($imagefunction['back'] === 'error' OR $imagefunction['back'] === 'error'):
                                    $imagebackurl = '/cardimg/back.jpg';
                                else:
                                    $imagebackurl = $imagefunction['back']."?$time";
                                endif;
                            endif;
                            // If the current record has null fields set the variables to 0 so updates
                            // from the Grid work.
                            // if (!isset($_POST["update"])) :    
                            if (empty($row['normal'])):
                                $myqty = 0;
                            else:
                                $myqty = $row['normal'];
                            endif;
                            if (empty($row['foil'])):
                                $myfoil = 0;
                            else:
                                $myfoil = $row['foil'];
                            endif;
                            if (empty($row['etched'])):
                                $myetch = 0;
                            else:
                                $myetch = $row['etched'];
                            endif;
                            $displayLang = strtoupper(htmlspecialchars($row['lang'], ENT_QUOTES, 'UTF-8'));
                                    
                            $msg->logMessage('[DEBUG]',"Collection view is $collection_view");
                            if(($myqty + $myfoil + $myetch) == 0 AND $collection_view == 1):
                                $in_collection = ' none no_collection';
                            elseif(($myqty + $myfoil + $myetch) == 0):
                                $in_collection = ' none';
                            elseif(($myqty + $myfoil + $myetch) > 0 AND $collection_view == 1):
                                $in_collection = '';
                            else:
                                $in_collection = '';
                            endif;
                            ?>
                            <div class='gridbox item'>
                                <?php
                                $msg->logMessage('[DEBUG]',"$imageurl");
                                if(in_array($row['layout'],$flip_button_cards)):
                                    echo "<div style='cursor: pointer;' class='flipbutton' onclick=swapImage(\"{$img_id}\",\"{$row['cs_id']}\",\"{$imageurl}\",\"{$imagebackurl}\")><span class='material-symbols-outlined refresh'>refresh</span></div>";
                                elseif($row['layout'] === 'flip'):
                                    echo "<div style='cursor: pointer;' class='flipbutton' onclick=rotateImg(\"{$img_id}\")><span class='material-symbols-outlined refresh'>refresh</span></div>";
                                endif;
                                $setname = htmlspecialchars($row['set_name'], ENT_QUOTES);
                                $number_import = $row['number_import'];
                                echo "<a class='gridlink' href='/carddetail.php?id=$scryid'><img id='$img_id' title='$uppercasesetcode ($setname / $displayLang) no. $number_import' class='card-image cardimg$in_collection' alt='$scryid' src='$imageurl'></a>";
                                $cellid = "cell".$scryid;
                                $cellid_one = $cellid.'_one';
                                $cellid_two = $cellid.'_two';
                                $cellid_three = $cellid.'_three';
                                $cellid_one_flash = $cellid_one;
                                $cellid_two_flash = $cellid_two;
                                $cellid_three_flash = $cellid_three;
                                ?>
                                <table class='bulksubmittable'>
                                    <tr class='bulksubmitrow'>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_one"; ?>">
                                            <?php
                                            if($meld === 'meld_result'):
                                                echo "Meld card";
                                            elseif ($not_paper == true):
                                                echo "<i>MtG Arena/Online</i>";
                                            elseif ($cardtypes === 'foilonly'):
                                                $poststring = 'newfoil';
                                                echo "Foil: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myfoil' value='$myfoil' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myfoil\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            elseif ($cardtypes === 'etchedonly'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myfoil' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myetch\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                $poststring = 'newqty';
                                                echo "Normal: <input class='bulkinput' id='$cellid_one' type='number' step='1' min='0' name='myqty' value='$myqty' onchange='ajaxUpdate(\"$scryid\",\"$cellid_one\",\"$myqty\",\"$cellid_one_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            endif;?>
                                        </td>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_two"; ?>">
                                            <?php
                                            if($meld === 'meld_result'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'foilonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'normalonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'etchedonly'):
                                                echo "&nbsp;";
                                            elseif ($cardtypes === 'normaletched'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_two' type='number' step='1' min='0' name='myetch' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_two\",\"$myetch\",\"$cellid_two_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                $poststring = 'newfoil';
                                                echo "Foil: <input class='bulkinput' id='$cellid_two' type='number' step='1' min='0' name='myfoil' value='$myfoil' onchange='ajaxUpdate(\"$scryid\",\"$cellid_two\",\"$myfoil\",\"$cellid_two_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            endif;?>
                                        </td>
                                        <td class='bulksubmittd' id="<?php echo $cellid."td_three"; ?>">
                                            <?php
                                            if ($cardtypes === 'normalfoiletched'):
                                                $poststring = 'newetch';
                                                echo "Etch: <input class='bulkinput' id='$cellid_three' type='number' step='1' min='0' name='myetch' value='$myetch' onchange='ajaxUpdate(\"$scryid\",\"$cellid_three\",\"$myetch\",\"$cellid_three_flash\",\"$poststring\");'>";
                                                echo "<input class='card' type='hidden' name='card' value='$scryid'>";
                                            else:
                                                echo "&nbsp;";
                                            endif;?>
                                        </td>
                                    </tr>
                                </table>
                            </div>    
                  <?php endwhile; ?>
                        <div class="ias-no-more">NO MORE RESULTS</div>
                        <div class="spinner"><img src='/images/ajax-loader.gif' alt="LOADING"></div>
                        <!--page navigation-->
                        <?php
                        if (isset($next)):
                            $getString = getStringParameters($_GET, 'page');
                            ?>
                            <div class="pagination"> <?php echo "<a href='index.php{$getString}&amp;page=$next' class='next'>Next</a>"; ?>
                            </div>
                  <?php endif ?>
                        <table class='bottompad'>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>    
                    </div>
                    <?php
                endif;
            else :
                $msg->logMessage('[DEBUG]',"Loading search layout");
                require('includes/search.php');
            endif;
            ?>
        </div> <?php 
        require('includes/footer.php'); 
        $msg->logMessage('[DEBUG]',"Finished");?>
        <script>
            if ('serviceWorker' in navigator) {
              window.addEventListener('load', function() {
                navigator.serviceWorker.register('/service-worker.js')
                  .then(function(registration) {
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                  })
                  .catch(function(err) {
                    console.log('ServiceWorker registration failed: ', err);
                  });
              });
            };

            document.getElementById("collQtyOp").addEventListener("change", function() {
                let qtySelect = document.getElementById("collQtyValue");
                let selectedOp = this.value;

                // Loop through options in collQtyValue
                for (let option of qtySelect.options) {
                    if (option.value === "1") {
                        option.disabled = (selectedOp === "ltn"); // Disable if "Less than" is selected
                    }
                }

                // Reset selection if the currently selected option is now disabled
                if (qtySelect.value === "1" && selectedOp === "ltn") {
                    qtySelect.value = ""; // Reset selection
                }
            });

            $(document).ready(function() {
                function updateVisibility() {
                    if ($('#cb1').is(':checked') || $('#abilitymain').is(':checked')) {
                        $('#exactbox').prop('disabled', false); // Enable checkbox
                    } else {
                        $('#exactbox').prop('checked', false).prop('disabled', true); // Uncheck and disable
                    }
                }

                // Trigger when checkboxes change
                $('#cb1, #abilitymain').change(updateVisibility);

                // Initialize on page load
                updateVisibility();
                function updateCollQtyState() {
                    if ($('input[name="scope"]:checked').val() === "mycollection") {
                        $('#collqtyspan select').prop('disabled', false).removeClass('disabled');
                    } else {
                        $('#collqtyspan select').prop('disabled', true).val("").addClass('disabled'); // Clear selection and disable
                    }
                }

                // Event listener for radio buttons
                $('input[name="scope"]').change(updateCollQtyState);

                // Initialize state on page load
                updateCollQtyState();
            });
        </script>
    </body>
</html>
