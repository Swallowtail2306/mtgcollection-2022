<?php 
/* Version:     4.2
    Date:       20/01/24
    Name:       sets.php
    Purpose:    Lists all setcodes and sets in the database
    Notes:      This page is the only one NOT mobile responsive design. 
 *              This is because the only way to access it is from the
                link on profile.php, from a <div> that is not visible on mobile.
 *  To do:      -
        
    1.0
                Initial version
    2.0         
                Moved to use Mysqli_Manager library
 *  3.0
 *              Refactoring for cards_scry
 *
 *  4.0         02/12/2023
 *              Add pagination and set image reload for admins
 * 
 *  4.1         20/01/24
 *              Move to logMessage
 * 
 *  4.2         29/05/24
 *              Fix incorrect set ordering
*/

if (file_exists('includes/sessionname.local.php')):
    require('includes/sessionname.local.php');
else:
    require('includes/sessionname_template.php');
endif;
startCustomSession();
require ('includes/ini.php');               //Initialise and load ini file
require ('includes/error_handling.php');
require ('includes/functions.php');     //Includes basic functions for non-secure pages
require ('includes/secpagesetup.php');      //Setup page variables
forcechgpwd();                              //Check if user is disabled or needs to change password
$msg = new Message($logfile);

?> 
<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1">
    <title><?php echo $siteTitle;?> - sets</title>
    <link rel="manifest" href="manifest.json" />
    <link rel="stylesheet" type="text/css" href="css/style<?php echo $cssver?>.css">
    <link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
    <?php include('includes/googlefonts.php');?>
    <script src="/js/jquery.js"></script>
    <?php
    $page = isset($_GET['page']) ? max(1, intval($_GET['page'])) : 1;
    $setsPerPage = 30; // Adjust this value based on your preference
    $range = 4; // Number of page results to show around current page in pagination
    
    // Get total sets count
    $totalSetsQuery = $db->query("SELECT COUNT(DISTINCT name) as totalSets FROM sets");
    $totalSets = $totalSetsQuery->fetch_assoc()['totalSets'];
    $totalPages = ceil($totalSets / $setsPerPage);
    
    if ($page > $totalPages):
        header("Location: /sets.php?page=" . max(1, $totalPages));
        exit;
    endif;
    
    ?>
    <script>
        function reloadImages(setcode) {
            document.body.style.cursor = "wait";
            $.ajax({
                type: 'POST',
                url: 'ajax/ajaxsetimg.php',
                data: { setcode: setcode },
                success: function(response) {
                    // Parse the JSON response
                    var result = JSON.parse(response);

                    // Display the message using an alert box
                    showMessage(result.status, result.message);

                    if (result.status === 'success') {
                        console.log(result.message);
                    } else {
                        console.error(result.message);
                    }
                    document.body.style.cursor = "default";
                },
                error: function(error) {
                    // Display an error message using an alert box
                    showMessage('error', 'An error occurred.');
                    console.error(error);
                    document.body.style.cursor = "default";
                }
            });
        };

        function showMessage(status, message) {
            // Display the message using an alert box
            alert(message);
        };
        
        // Function to send an AJAX request to filter sets
        var isAdmin = <?php echo json_encode($admin == 1); ?>;
        
        function buildPagination(totalPages, currentPage, setsPerPage) {
            console.log('Total: ' + totalPages + ', Current: ' + currentPage);
            var paginationHTML = '';
            paginationHTML += '<br>Page &nbsp;';
            var range = <?php echo $range; ?>; // Number of pages to show before and after the current page
            
            if (currentPage !== 1) {
                paginationHTML += buildPageLink('previous', currentPage, setsPerPage);
            }

            for (var i = 1; i <= totalPages; i++) {
                if (i === 1 || i === totalPages || (i >= currentPage - range && i <= currentPage + range)) {
                    paginationHTML += buildPageLink(i, currentPage, setsPerPage);
                } else if ((i === currentPage - range - 1 || i === currentPage + range + 1)) {
                    paginationHTML += '<span class="pagination-item">...&nbsp;</span>';
                }
            }
            
            if (currentPage !== totalPages) {
                paginationHTML += buildPageLink('next', currentPage, setsPerPage);
            }
            
            paginationHTML += '<br>&nbsp;';
            return paginationHTML;
        };

        function buildPageLink(page, currentPage, setsPerPage) {
            if (page === currentPage) {
                return '<span class="pagination-item" style="font-weight: bold">' + page + '&nbsp;&nbsp;</span>';
            } else if (page === 'next') {
                var nextPage = currentPage + 1;
                return '<a class="pagination-item" href="javascript:loadPage(' + nextPage + ', ' + setsPerPage + ');"><span class="material-symbols-outlined set-page pagination-item">skip_next</span></a>';
            } else if (page === 'previous') {
                var previousPage = currentPage - 1;
                return '<a class="pagination-item" href="javascript:loadPage(' + previousPage + ', ' + setsPerPage + ');"><span class="material-symbols-outlined set-page pagination-item">skip_previous</span></a>&nbsp;';
            } else {
                return '<a class="pagination-item" href="javascript:loadPage(' + page + ', ' + setsPerPage + ');">' + page + '&nbsp;&nbsp;</a>';
            }
        };

        function fetchAndDisplaySets(filterValue, pageNumber, setsPerPage) {
            var offset = (pageNumber * setsPerPage) - (setsPerPage);
            offset = Math.max(0, offset);

            $.ajax({
                type: 'GET',
                url: 'ajax/ajaxsets.php',
                data: { filter: filterValue, setsPerPage: setsPerPage, offset: offset },
                dataType: 'json',
                success: function(response) {
                    if (response.numResults === 0) {
                        document.getElementById('setlist').style = "display: none";
                        document.getElementById('paginationTop').style = "display: none";
                        document.getElementById('paginationBottom').style = "display: none";
                        document.getElementById('noResults').style = "display: block";
                        console.log("Set search: No results");
                    } else if (response.numPages === 1) {
                        document.getElementById('setlist').style = "display: table";
                        updateTable(response.filteredSets);
                        window.scrollTo(0,0);
                        document.getElementById('paginationTop').style = "display: none";
                        document.getElementById('paginationBottom').style = "display: none";
                        document.getElementById('noResults').style = "display: none";
                        console.log("Set search: Results: " + response.numResults + "; Pages: " + response.numPages + "; Page: " + pageNumber);
                    } else {
                        document.getElementById('setlist').style = "display: table";
                        updateTable(response.filteredSets);
                        window.scrollTo(0,0);
                        document.getElementById('paginationTop').style = "display: block";
                        document.getElementById('paginationBottom').style = "display: block";
                        document.getElementById('noResults').style = "display: none";
                        var paginationHTML = buildPagination(response.numPages, pageNumber, setsPerPage);
                        document.getElementById('paginationTop').innerHTML = paginationHTML;
                        document.getElementById('paginationBottom').innerHTML = paginationHTML;
                        console.log("Set search: Results: " + response.numResults + "; Pages: " + response.numPages + "; Page: " + pageNumber);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error("Set search: AJAX error: " + textStatus + " - " + errorThrown);
                }
            });
        };

        function loadPage(pageNumber, setsPerPage) {
            var filterValue = document.getElementById('setCodeFilter').value;
            fetchAndDisplaySets(filterValue, pageNumber, setsPerPage);
        };
        
        var debounceTimer;
        
        function filterSets() {
            var filterValue = document.getElementById('setCodeFilter').value;
            var setsPerPage = <?php echo $setsPerPage; ?>;
            
            // Clear the previous timer if it exists
            clearTimeout(debounceTimer);
            
            debounceTimer = setTimeout(function() {
                if (filterValue.length >= 3 || filterValue.length === 0) {
                    fetchAndDisplaySets(filterValue, 1, setsPerPage);
                }
            }, 300); // Adjust the delay (in milliseconds) as needed
        };

        // Function to update the table with filtered results
        function updateTable(filteredSets) {
            var table = document.querySelector('#setlist');
            var tableBody = table.getElementsByTagName('tbody')[0];
            var currentYear = ''; // Variable to keep track of the current year
            var totalColumns = isAdmin ? 8 : 7; // Total columns in the table

            while (tableBody.rows.length > 1) {
                tableBody.deleteRow(1);
            }

            filteredSets.forEach(function (set) {
                var setYear = new Date(set.setdate).getFullYear().toString();
                if (setYear != currentYear) {
                    var yearRow = tableBody.insertRow(tableBody.rows.length);
                    var yearCell = yearRow.insertCell(0);
                    yearCell.colSpan = totalColumns;
                    yearCell.className = "year-header";
                    yearCell.innerHTML = '<h3>' + setYear + '</h3>';
                    currentYear = setYear;
                }

                var row = tableBody.insertRow(tableBody.rows.length);
                // Populate the row with data from set
                var iconCell = row.insertCell(0);
                var setcode = set.setcode;
                var time = new Date().getTime(); // To ensure fresh image load
                var img = document.createElement('img');
                img.className = 'seticon';
                img.src = 'cardimg/seticons/' + setcode + '.svg?' + time;
                img.alt = setcode.toUpperCase();
                iconCell.appendChild(img);

                var codeCell = row.insertCell(1);
                var setcodeupper = set.setcode.toUpperCase();
                var link = document.createElement('a');
                link.href = 'index.php?complex=yes&searchname=yes&legal=any&set%5B%5D=' + encodeURIComponent(setcodeupper) + '&sortBy=setdown&layout=grid';
                link.textContent = setcodeupper;
                codeCell.appendChild(link);

                var nameCell = row.insertCell(2);
                nameCell.textContent = set.set_name;

                var typeCell = row.insertCell(3);
                var setType = set.set_type.split('_').map(function(word) {
                    return word.charAt(0).toUpperCase() + word.slice(1);
                    }).join(' ');
                setType = setType.replace(/_/g, ' ');
                typeCell.textContent = setType;
                typeCell.classList.add('columnhide');

                var parentCell = row.insertCell(4);
                parentCell.textContent = set.parent_set_code.toUpperCase();
                parentCell.classList.add('columnhide');

                var dateCell = row.insertCell(5);
                var inputDate = set.setdate; // Replace set.setdate with your date variable

                // Split the input date into components
                var dateComponents = inputDate.split('-');
                var year = dateComponents[0];
                var month = dateComponents[1];
                var day = parseInt(dateComponents[2]);

                // Create an array of month names
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                // Format the date as "30 Oct 2010" and set it in dateCell
                dateCell.textContent = monthNames[parseInt(month) - 1] + " " + day;

                var countCell = row.insertCell(6);
                countCell.textContent = set.card_count.toLocaleString();
                countCell.classList.add('columnhide');

                if (isAdmin) {
                    var reloadCell = row.insertCell(7);
                    reloadCell.style.textAlign = 'center';

                    var link = document.createElement('a');
                    link.href = "javascript:void(0);";
                    link.onclick = function() {
                        reloadImages(set.setcode);
                    };

                    var iconSpan = document.createElement('span');
                    iconSpan.className = "material-symbols-outlined";
                    iconSpan.textContent = "frame_reload";

                    link.appendChild(iconSpan);
                    reloadCell.appendChild(link);
                }
            });
        };
        
        $(document).ready(function() {
            $('#setCodeFilter').focus();
        });
    </script>
</head>

<body class="body">
<?php 
include_once("includes/analyticstracking.php");
require('includes/overlays.php');             
require('includes/header.php');
require('includes/menu.php'); 
?>
    
<div id='page'>
    <div class='staticpagecontent'>
        <?php
        $sql = "SELECT 
                    name as set_name,
                    code as setcode,
                    parent_set_code,
                    set_type,
                    card_count,
                    nonfoil_only,
                    foil_only,
                    min(release_date) as date,
                    release_date as setdate
                FROM sets 
                GROUP BY 
                    name
                ORDER BY 
                    setdate DESC, length(setcode) ASC, length(parent_set_code) ASC, parent_set_code DESC, setcode ASC
                LIMIT ?";
        $stmt = $db->prepare($sql);
        $msg->logMessage('[DEBUG]',"Query is: $sql");
        $stmt->bind_param("i", $setsPerPage);
        if ($stmt === false):
            trigger_error("[ERROR] ".basename(__FILE__)." ".__LINE__,": Preparing SQL: " . $db->error, E_USER_ERROR);
        endif;
        $exec = $stmt->execute();
        if ($exec === false):
            trigger_error("[ERROR] ".basename(__FILE__)." ".__LINE__,": Executing SQL: " . $db->error, E_USER_ERROR);
        else: 
            $result = $stmt->get_result();
            if ($result->num_rows === 0):
                trigger_error('[ERROR]'.basename(__FILE__)." ".__LINE__.": No results ". $db->error, E_USER_ERROR);
            endif;
        endif;
        ?>
        <div class="sets-header-container">
            <h2 class='h2pad sets-header'>Sets</h2>
            <div class="filter-container">
                <input type="text" class="textinput" id="setCodeFilter" oninput="filterSets(this.value, <?php echo $setsPerPage; ?>)" placeholder="NAME/CODE/YEAR FILTER">
                <div id='cancelsetfilter'><span class="material-symbols-outlined">close</span></div>
            </div>
        </div> <?php
        echo '<div id="paginationTop" class="pagination" style="display: block;"><br>Page &nbsp;';
        for ($i = 1; $i <= $totalPages; $i++):
            if ($i == 1 || $i == $totalPages || ($i >= $page - $range && $i <= $page + $range)):
                // Highlight the current page, or add a link for other pages
                if ($i == $page):
                    echo '<span class="pagination-item" style="font-weight: bold">' . $i . '&nbsp;&nbsp;</span>';
                else:
                    echo '<a class="pagination-item" href="javascript:loadPage(' . $i . ', ' . $setsPerPage . ');">' . $i . '&nbsp;&nbsp;</a>';
                endif;
            elseif ($i === $page - $range - 1 || $i === $page + $range + 1):
                echo '<span class="pagination-item">...&nbsp;</span>';
            endif;
        endfor;
        echo '<a class="pagination-item" href="javascript:loadPage(' . $page + 1 . ', ' . $setsPerPage . ');"><span class="material-symbols-outlined set-page pagination-item">skip_next</span></a>';
        echo '<br>&nbsp;</div>'; ?>
        <table id='setlist'>
            <tr>
                <td class='setcell'>
                    <b>Icon</b>
                </td>
                <td class='setcell'>
                    <b>Code</b>
                </td>
                <td class='setcell'>
                    <b>Name</b>
                </td>
                <td class='setcell columnhide'>
                    <b>Type</b>
                </td>
                <td class='setcell columnhide'>
                    <b>Parent set</b>
                </td>
                <td class='setcell'>
                    <b>Release date</b>
                </td>
                <td class='setcell columnhide'>
                    <b>Card count</b>
                </td>
                <?php if ($admin == 1): ?>
                    <td class='setcell'>
                        <b>Reload images</b>
                    </td>
                <?php endif; ?>
            </tr>
            <?php
            $currentYear = null;
            if($result === false):
                // Should never get here with catches above
                $msg->logMessage('[ERROR]',"Error retrieving data"); ?>
                <tr>
                    <td colspan="2">Error retrieving data</td>
                </tr> <?php
            else:
                while ($row = $result->fetch_assoc()): 
                    $setYear = date('Y', strtotime($row['setdate']));
                    if(isset($row['setcode']) AND $row['setcode'] !== null):
                        $setcodeupper = strtoupper($row['setcode']);
                    else:
                        $setcodeupper = '';
                    endif;
                    if(isset($row['set_name']) AND $row['set_name'] !== null):
                        $setname = $row['set_name'];
                    else:
                        $setname = '';
                    endif;
                    if(isset($row['set_type']) AND $row['set_type'] !== null):
                        $settype = ucwords(str_replace('_', ' ', $row['set_type']));
                    else:
                        $settype = '';
                    endif;
                    if(isset($row['parent_set_code']) AND $row['parent_set_code'] !== null):
                        $parentsetcode = strtoupper($row['parent_set_code']);
                    else:
                        $parentsetcode = '';
                    endif;
                    if(isset($row['setdate']) AND $row['setdate'] !== null):
                        $setdate = strtoupper($row['setdate']);
                    else:
                        $setdate = '';
                    endif;
                    if(isset($row['card_count']) AND $row['card_count'] !== null):
                        $cardcount = strtoupper($row['card_count']);
                    else:
                        $cardcount = '';
                    endif;
                    if ($setYear != $currentYear):
                        echo '<tr>';
                        if ($admin == 1):
                            echo '<td colspan="8" class="year-header"><h3>' . $setYear . '</h3></td>';
                        else:
                            echo '<td colspan="7" class="year-header"><h3>' . $setYear . '</h3></td>';
                        endif;
                        echo '</tr>';
                        $currentYear = $setYear;
                    endif;

                    ?>
                    <tr>
                        <td class='setcell'>
                            <?php 
                            $time = time();
                            echo "<img class='seticon' src='cardimg/seticons/{$row['setcode']}.svg?$time' alt='$setcodeupper'>"; ?>
                        </td>
                        <td class='setcell'>
                            <?php echo "<a href='index.php?complex=yes&amp;searchname=yes&amp;legal=any&amp;set%5B%5D=$setcodeupper&amp;sortBy=setdown&amp;layout=grid'>$setcodeupper</a>"; ?>
                        </td>
                        <td class='setcell'>
                            <?php echo $setname; ?>
                        </td>
                        <td class='setcell columnhide'>
                            <?php echo $settype; ?>
                        </td>
                        <td class='setcell columnhide'>
                            <?php echo $parentsetcode; ?>
                        </td>
                        <td class='setcell'>
                            <?php echo date('M j', strtotime($setdate)); ?>
                        </td>
                        <td class='setcell columnhide' style='text-align: center;'>
                            <?php echo number_format($cardcount); ?>
                        </td>
                        <td class='setcell' style='text-align: center;'>
                            <?php echo ($admin == 1 ? '<a href="javascript:void(0);" onclick="reloadImages(\''.$row['setcode'].'\')"><span class="material-symbols-outlined">frame_reload</span></a>' : ''); ?>
                        </td>
                    </tr>
                    <?php 
                endwhile;
            endif;
            ?>
        </table> <?php
        echo '<div id="noResults" style="display:none"><br>No results<br></div>';
        echo '<div id="paginationBottom" class="pagination"><br>Page &nbsp;';
        for ($i = 1; $i <= $totalPages; $i++):
            if ($i == 1 || $i == $totalPages || ($i >= $page - $range && $i <= $page + $range)):
                // Highlight the current page, or add a link for other pages
                if ($i == $page):
                    echo '<span class="pagination-item" style="font-weight: bold">' . $i . '&nbsp;&nbsp;</span>';
                else:
                    echo '<a class="pagination-item" href="javascript:loadPage(' . $i . ', ' . $setsPerPage . ');">' . $i . '&nbsp;&nbsp;</a>';
                endif;
            elseif ($i === $page - $range - 1 || $i === $page + $range + 1):
                echo '<span class="pagination-item">...&nbsp;</span>';
            endif;
        endfor;
        echo '<a class="pagination-item" href="javascript:loadPage(' . $page + 1 . ', ' . $setsPerPage . ');"><span class="material-symbols-outlined set-page pagination-item">skip_next</span></a>';
        echo '</div>';
        ?>
        <br>&nbsp;
    </div>echo '<br>&nbsp;</div>';
</div>
<?php     
    require('includes/footer.php'); 
?>
    <script>
        document.getElementById('cancelsetfilter').addEventListener('click', function() {
            document.getElementById('setCodeFilter').value = '';
            var filterValue = document.getElementById('setCodeFilter').value;
            var setsPerPage = <?php echo $setsPerPage; ?>;
            fetchAndDisplaySets(filterValue, 1, setsPerPage);
        });
    </script>
</body>
</html>