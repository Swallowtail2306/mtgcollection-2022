# Redirect http requests to https, except for LetsEncrypt validation requests

<VirtualHost 192.168.1.245:80>
    ErrorLog logs/error_log
    CustomLog logs/obelix_log combined
    LogLevel warn

    ServerName obelix.simonandkate.lan

    DocumentRoot /var/www/mtgnew
    RedirectMatch 301 ^/((?!.well-known/acme-challenge/).*)$ https://obelix.simonandkate.lan/$1
</VirtualHost>

<VirtualHost 192.168.1.245:443>
    ErrorLog logs/ssl_error_log
    CustomLog logs/obelix_log combined
    LogLevel warn
    ErrorDocument 500 /error.php

    DocumentRoot /var/www/mtgnew

    ServerName obelix.simonandkate.lan
    KeepAlive On

    <Files ~ "\.ini$">
        Order allow,deny
        Deny from all
    </Files>

    Alias  /cardimg/  /mnt/data/cardimg/

    <Directory /mnt/data/cardimg/>
        Require all granted
    </Directory>    

    <Directory /var/www/mtgnew>
        Options -Indexes
        Header set X-Robots-Tag "noindex"
        FileETag MTime Size
        AddType image/x-icon .ico
        ExpiresActive On
        ExpiresByType image/gif "access plus 1 months"
        ExpiresByType image/jpg "access plus 1 months"
        ExpiresByType image/jpeg "access plus 1 months"
        ExpiresByType image/png "access plus 1 months"
        ExpiresByType image/svg+xml "access plus 1 months"
        ExpiresByType image/vnd.microsoft.icon "access plus 1 months"
        ExpiresByType application/javascript "access plus 1 months"
        ExpiresByType application/x-javascript "access plus 1 months"
        ExpiresByType text/javascript "access plus 1 months"
        ExpiresByType text/plain "access plus 1 year"
        ExpiresByType image/x-icon "access plus 1 year"
        ExpiresByType image/ico "access plus 1 year"
        ExpiresByType text/css "access plus 1 hour"
    </Directory> 

    <Directory "/var/www/mtgnew/bulk">
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
    </Directory>

    <Directory "/var/www/mtgnew/setup">
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
    </Directory>

    <Location "/images">
        ExpiresActive On
        ExpiresDefault "access plus 1 month"
    </Location>

    <Location "/cardimg">
        ExpiresActive On
        ExpiresDefault "access plus 1 month"
    </Location>
    <Location "/cardimg/deck_photos">
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
    </Location>

    SSLEngine on
    SSLProtocol             all -SSLv3 -TLSv1 -TLSv1.1
    SSLCipherSuite          ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
    SSLHonorCipherOrder     off
    SSLSessionTickets       off

    Protocols h2 http/1.1
    Header always set Strict-Transport-Security "max-age=63072000;"
    Header edit Set-Cookie ^(.*)$ $1;HttpOnly;Secure

    SSLCertificateFile /etc/letsencrypt/live/asterix.mtgcollection.info/cert.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/asterix.mtgcollection.info/privkey.pem
    SSLCertificateChainFile /etc/letsencrypt/live/asterix.mtgcollection.info/chain.pem
    Include /etc/letsencrypt/options-ssl-apache.conf

    #Set to gzip all output
    SetOutputFilter DEFLATE

    #exclude the following file types
    SetEnvIfNoCase Request_URI \.(?:exe|t?gz|zip|iso|tar|bz2|sit|rar|png|jpg|gif|jpeg|flv|swf|mp3)$ no-gzip dont-vary

    #set compression level
    DeflateCompressionLevel 9

    #Handle browser specific compression requirements
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4.0[678] no-gzip
    BrowserMatch bMSIE !no-gzip !gzip-only-text/html
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0

</VirtualHost>